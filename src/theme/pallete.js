export default {
  yellow: '#f9c635',
  orange: '#f45d27',
  brown: '#55330d',
  brownLight: '#6d4a05',
  brownDark: '#cacbce',
  purpleDark: '#2b2c2e',
  tealLight: '#437a88',
  beige: '#dbce74',
};
