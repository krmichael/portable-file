import React from 'react';

import colors from './pallete';

export default React.createContext({
  colors,
});
