import React, { useContext, useRef } from 'react';
import Drawer from 'react-native-gesture-handler/DrawerLayout';

import {
  StatusBar,
  /* Dimensions, */
} from 'react-native';

import { Container } from './styles';

import ThemeContext from '../../theme';

import Header from '../../components/Header';
import Search from '../../components/Search';
import SubHeader from '../../components/SubHeader';
import Content from '../../components/Content';
import ContentFolder from '../../components/ContentFolder';
import Footer from '../../components/Footer';

export default function Home() {
  const { colors } = useContext(ThemeContext);
  const drawerRef = useRef(null);
  //const { width, height } = Dimensions.get('window');

  return (
    <>
      <StatusBar
        backgroundColor={colors.yellow}
        barStyle="dark-content"
        hidden={true}
      />
      <Container>
        <Drawer
          ref={drawerRef}
          drawerWidth={340}
          drawerPosition={'right'}
          drawerType={'front'}
          drawerBackgroundColor={'transparent'}
          overlayColor={'transparent'}
          renderNavigationView={ContentFolder}>
          <>
            <Header />
            <Search />
            <SubHeader />
            <Content />
            <Footer />
          </>
        </Drawer>
      </Container>
    </>
  );
}
