import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';

//import colors from '../../theme/pallete';

export const Container = styled(LinearGradient).attrs({
  colors: [
    '#d4ab11',
    '#fddf39',
    '#ffe850',
    '#ffed79',
    '#fef094',
    '#f5f1d7',
    '#f9f9fa',
    '#f3f4f6',
    '#ffffff',
  ],
})`
  flex: 1;
`;
