import React from 'react';
import { ThemeProvider } from 'styled-components';

import Home from './pages/Home';

import ThemeContext from './theme';
import colors from './theme/pallete';

export default function App() {
  return (
    <ThemeContext.Provider value={{ colors }}>
      <ThemeContext.Consumer>
        {(theme) => (
          <ThemeProvider theme={theme.colors}>
            <Home />
          </ThemeProvider>
        )}
      </ThemeContext.Consumer>
    </ThemeContext.Provider>
  );
}
