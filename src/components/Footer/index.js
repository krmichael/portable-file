import React, { useContext } from 'react';

import { Container, Folder, Add, Compass } from './styles';
import ThemeContext from '../../theme';

export default function Footer() {
  const { colors } = useContext(ThemeContext);

  return (
    <Container>
      <Folder name="ios-folder-open" size={30} color={colors.yellow} />
      <Add name="md-add" size={30} color={colors.purpleDark} />
      <Compass name="compass-alt" size={30} color={colors.purpleDark} />
    </Container>
  );
}
