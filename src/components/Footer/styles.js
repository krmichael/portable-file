import { Platform } from 'react-native';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/Ionicons';
import CompassIcon from 'react-native-vector-icons/Fontisto';

export const Container = styled.View.attrs({
  elevation: 2,
  ...Platform.select({
    ios: {
      shadowOpacity: 0.3,
      shadowRadius: 3,
      shadowOffset: {
        width: 0,
        height: 0,
      },
    },
  }),
})`
  position: absolute;
  flex-direction: row;
  justify-content: space-around;
  bottom: 0px;
  background-color: #fff;
  padding: 20px 15px;
  width: 100%;
`;

export const Folder = styled(Icon)``;

export const Add = styled(Icon)``;

export const Compass = styled(CompassIcon)``;
