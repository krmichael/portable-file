import React, { useContext } from 'react';

import { Container, Input, SearchIcon } from './styles';

import ThemeContext from '../../theme';

export default function Search() {
  const { colors } = useContext(ThemeContext);

  return (
    <Container>
      <Input />
      <SearchIcon name="search" size={25} color={colors.purpleDark} />
    </Container>
  );
}
