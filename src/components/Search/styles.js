import styled from 'styled-components/native';
import Icon from '../IonIcon';

export const Container = styled.View`
  position: relative;
  justify-content: center;
  margin-top: 20px;
  padding: 0px 15px;
`;

export const Input = styled.TextInput`
  background-color: #fffadf;
  margin: 0px 10px;
  padding-left: 7px;
  padding-right: 7px;
  border-radius: 5px;
`;

export const SearchIcon = styled(Icon)`
  position: absolute;
  right: 0px;
  margin-right: 35px;
`;
