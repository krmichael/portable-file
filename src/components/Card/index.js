import React, { useContext } from 'react';

import { Container, Folder, Scope, Description, CreatedAt } from './styles';

import ThemeContext from '../../theme';

export default function Card({ icon, description, createdAt, color }) {
  const { colors } = useContext(ThemeContext);

  return (
    <Container>
      <Folder
        name={icon || 'ios-folder-open'}
        size={60}
        color={color || colors.tealLight}
      />

      <Scope>
        <Description>{description || 'Important file'}</Description>

        <CreatedAt>{createdAt || '20.02.2020'}</CreatedAt>
      </Scope>
    </Container>
  );
}
