import { Animated, StyleSheet } from 'react-native';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/Ionicons';

export const Container = styled(Animated.View)`
  width: 45%;
  height: 180px;
  background-color: #fefefe;
  border: ${StyleSheet.hairlineWidth}px solid #f2f2f2;
  border-radius: 10px;
  padding: 25px 0px 30px 15px;
  margin: 15px 5px;
  justify-content: space-between;
`;

export const Folder = styled(Icon)``;

export const Scope = styled.View``;

export const Description = styled.Text`
  font-size: 16px;
  font-weight: bold;
  letter-spacing: 0.5px;
  color: ${(props) => props.theme.purpleDark};
`;

export const CreatedAt = styled.Text`
  margin-top: 5px;
  font-size: 12px;
  font-weight: 600;
  color: ${(props) => props.theme.brownDark};
`;
