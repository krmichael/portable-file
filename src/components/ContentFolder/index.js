import React, { useContext } from 'react';
import { Animated, Dimensions } from 'react-native';

import { PanGestureHandler, State } from 'react-native-gesture-handler';

import ExcelIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import ThemeContext from '../../theme';

import {
  Container,
  Content,
  ScopeStrap,
  Strap,
  Header,
  Options,
  Menu,
  Folder,
  Scope,
  Description,
  TotalFiles,
  List,
} from './styles';

import * as SCard from '../Card/styles';

const CARDS_FILES = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28baa',
    description: 'Project offer',
    color: '#51c4a0',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    description: 'File Move',
    color: '#f9c635',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    description: 'Finance',
    color: '#876dfa',
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    description: 'Source file',
    color: '#71d42c',
  },
];

const maxHeight = Dimensions.get('window').height - 200;

//@refresh reset
function Card({ icon, description, createdAt, color }) {
  const { colors } = useContext(ThemeContext);

  const translateX = new Animated.Value(0);
  const translateY = new Animated.Value(0);

  const animatedEvent = Animated.event(
    [
      {
        nativeEvent: {
          translationX: translateX,
          translationY: translateY,
        },
      },
    ],
    { useNativeDriver: true },
  );

  function handlerStateChange(event) {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      const { translationX, translationY } = event.nativeEvent;

      if (translationX) {
        Animated.timing(translateX, {
          toValue: 0,
          duration: 250,
          useNativeDriver: true,
        }).start();
      }

      if (translationY) {
        Animated.timing(translateY, {
          toValue: 0,
          duration: 250,
          useNativeDriver: true,
        }).start();
      }
    }
  }

  return (
    <PanGestureHandler
      onGestureEvent={animatedEvent}
      onHandlerStateChange={handlerStateChange}>
      <SCard.Container
        style={{
          transform: [
            {
              translateX: translateX.interpolate({
                inputRange: [0, 40],
                outputRange: [0, 40],
                extrapolateRight: 'clamp',
                extrapolateLeft: 'extend',
              }),
            },
            {
              translateY: translateY.interpolate({
                inputRange: [-maxHeight, 0, 50],
                outputRange: [-maxHeight, 0, 30],
                extrapolate: 'clamp',
              }),
            },
          ],
        }}>
        <ExcelIcon
          name={'file-excel'}
          size={60}
          color={color || colors.tealLight}
        />

        <SCard.Scope>
          <SCard.Description>
            {description || 'Important file'}
          </SCard.Description>

          <SCard.CreatedAt>{createdAt || '20.02.2020'}</SCard.CreatedAt>
        </SCard.Scope>
      </SCard.Container>
    </PanGestureHandler>
  );
}

function renderFiles({ item }) {
  return (
    <Card key={item.id} description={item.description} color={item.color} />
  );
}

export default function ContentFolder() {
  return (
    <Container show>
      <ScopeStrap>
        <Strap />
      </ScopeStrap>

      <Content>
        <Header>
          <Options>
            <Menu />
          </Options>

          <Scope>
            <Folder />
            <Description>Document</Description>
            <TotalFiles>24 files in total</TotalFiles>
          </Scope>
        </Header>

        <List data={CARDS_FILES} renderItem={renderFiles} />
      </Content>
    </Container>
  );
}
