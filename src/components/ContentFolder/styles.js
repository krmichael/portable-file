import { Animated } from 'react-native';
import styled from 'styled-components/native';
import MenuIcon from 'react-native-vector-icons/Entypo';
import FolderIcon from 'react-native-vector-icons/Ionicons';

export const Container = styled.View`
  flex: 1;
  display: ${(props) => (props.show ? 'flex' : 'none')};
`;

export const Content = styled.View`
  flex: 1;
  background-color: #fff;
  border-top-left-radius: 45px;
`;

export const ScopeStrap = styled.View`
  position: absolute;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  left: -17px;
  width: 15px;
  height: 100%;
`;

export const Strap = styled.View`
  width: 7px;
  height: 50px;
  background-color: #ddd;
  border-radius: 2px;
`;

export const Header = styled.View`
  margin-top: 20px;
`;

export const Menu = styled(MenuIcon).attrs({
  name: 'dots-two-vertical',
  size: 30,
})`
  color: ${(props) => props.theme.purpleDark};
`;

export const Folder = styled(FolderIcon).attrs({
  name: 'ios-folder-open',
  size: 70,
})`
  color: ${(props) => props.theme.yellow};
`;

export const Options = styled.View`
  align-items: flex-end;
  margin-right: 10px;
`;

export const Scope = styled.View`
  padding: 10px;
  margin-top: 20px;
  justify-content: center;
  align-items: center;
`;

export const Description = styled.Text`
  font-size: 28px;
  font-weight: bold;
  margin-top: 10px;
  color: ${(props) => props.theme.purpleDark};
`;

export const TotalFiles = styled.Text`
  font-size: 15px;
  margin-top: 10px;
`;

export const List = styled(Animated.FlatList).attrs({
  showsVerticalScrollIndicator: false,
  numColumns: 2,
  contentContainerStyle: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  columnWrapperStyle: {
    justifyContent: 'space-between',
  },
})``;
