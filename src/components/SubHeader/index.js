import React, { useContext } from 'react';
import ArrowIcon from 'react-native-vector-icons/Ionicons';
import GridIcon from 'react-native-vector-icons/Entypo';

import { Container, Label } from './styles';
import ThemeContext from '../../theme';

export default function SubHeader() {
  const { colors } = useContext(ThemeContext);

  return (
    <Container>
      <Label>
        Recent{' '}
        <ArrowIcon name="ios-arrow-down" size={20} color={colors.purpleDark} />
      </Label>

      <GridIcon name="grid" size={30} color={colors.beige} />
    </Container>
  );
}
