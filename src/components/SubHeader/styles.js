import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 20px 10px 0px 10px;
  padding: 0px 15px;
`;

export const Label = styled.Text`
  font-size: 18px;
  font-weight: 600;
`;
