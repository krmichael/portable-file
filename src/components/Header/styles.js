import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 30px 15px;
  padding: 0px 15px;
`;

export const Title = styled.Text`
  font-size: 28px;
  font-weight: bold;
  color: ${(props) => props.theme.purpleDark};
`;

export const User = styled.Image`
  width: 55px;
  height: 55px;
  border-radius: 55px;
`;
