import React from 'react';

import { Container, Title, User } from './styles';
import me from '../../assets/me.png';

export default function Header() {
  return (
    <Container>
      <Title>Portable file</Title>

      <User source={me} />
    </Container>
  );
}
