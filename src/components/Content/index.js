import React from 'react';

import { Container, List } from './styles';
import Card from '../Card';

const CARDS_DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28baa',
    description: 'Important File',
    color: '#51c4a0',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    description: 'Document',
    color: '#f9c635',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    description: 'Principle',
    color: '#876dfa',
  },
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    description: 'Excel File',
    color: '#71d42c',
  },
];

function renderCards({ item }) {
  return (
    <Card key={item.id} description={item.description} color={item.color} />
  );
}

export default function Content() {
  return (
    <Container>
      <List data={CARDS_DATA} renderItem={renderCards} />
    </Container>
  );
}
