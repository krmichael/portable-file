import { FlatList } from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  padding-bottom: 60px;
`;

export const List = styled(FlatList).attrs({
  showsVerticalScrollIndicator: false,
  numColumns: 2,
  contentContainerStyle: {
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  columnWrapperStyle: {
    justifyContent: 'space-between',
  },
})``;
